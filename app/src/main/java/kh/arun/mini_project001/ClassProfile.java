package kh.arun.mini_project001;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class ClassProfile implements Serializable {
    String name;

    public ClassProfile(String name) {
        this.name = name;
    }

    public ClassProfile() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ClassProfile{" +
                "name='" + name + '\'' +
                '}';
    }

}


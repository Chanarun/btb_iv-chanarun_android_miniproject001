package kh.arun.mini_project001;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import java.nio.channels.InterruptedByTimeoutException;

public class Profile extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    Spinner spin;
    AppCompatEditText inputName;
    String[] job={"I am a customer", "I am a waiter", "I am a waitress", "I am a chef"};
    AppCompatButton btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        spin=findViewById(R.id.spinner);
        inputName=findViewById(R.id.inputName);
        btnSave=findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            private boolean vUsername() {
                String usernameInput = inputName.getText().toString().trim();

                if (usernameInput.isEmpty()) {
                    YoYo.with(Techniques.Shake)
                            .duration(700)
                            .repeat(0)
                            .playOn(inputName);
                    inputName.setError("Field can't be empty");
                    return false;
                } else if (usernameInput.length() > 20) {
                    YoYo.with(Techniques.Shake)
                            .duration(700)
                            .repeat(0)
                            .playOn(inputName);
                    inputName.setError("Username too long");
                    return false;
                } else {
                    inputName.setError(null);
                    return true;
                }
            }
            @Override
            public void onClick(View view) {


                String setName=inputName.getText().toString();
                if ( !vUsername() ) {
                    return;
                }


                Intent intent= new Intent(Profile.this, DetailProfile.class);
                ClassProfile user= new ClassProfile(setName);
                intent.putExtra("Information",user);
                startActivity(intent);


            }


        });



        spin.setOnItemSelectedListener(this);

        ArrayAdapter selectJob = new ArrayAdapter(this,android.R.layout.simple_spinner_item,job);
        selectJob.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin.setAdapter(selectJob);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
package kh.arun.mini_project001;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.GridLayout;
import android.widget.TextView;

import com.github.javafaker.Faker;

public class Notedbook extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notedbook);

        GridLayout gridLayout = findViewById(R.id.listbook);
        Faker faker = new Faker();
        for (int i = 0; i <= 99; i++) {
            View child = getLayoutInflater().inflate(R.layout.card, null);

            TextView tittle = child.findViewById(R.id.tittle);
            TextView des = child.findViewById(R.id.des);

            tittle.setText(faker.book().title());
            des.setText(faker.book().author());
            gridLayout.addView(child);
        }
    }
}
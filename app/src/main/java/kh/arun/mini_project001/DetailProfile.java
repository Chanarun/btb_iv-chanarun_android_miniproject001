package kh.arun.mini_project001;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

public class DetailProfile extends AppCompatActivity {
    TextView setName;
    RadioButton setGender;
    Spinner setJob;
    AppCompatButton btnback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_profile);
        setName=findViewById(R.id.setUsername);
        btnback=findViewById(R.id.btnToBack);

        ClassProfile user=(ClassProfile)getIntent().getSerializableExtra("Information");
        setName.setText(user.name);

        btnback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(DetailProfile.this,Profile.class);
                startActivity(intent);
            }
        });

    }
}